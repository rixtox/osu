require "net/http"
require "uri"
require 'mongo'
include Mongo

client = MongoClient.new
db = client['osu']
songs_db = db['songs']
maps_db = db['maps']

conf = {
  name: '',
  pass: ''
}

song_dir = ARGV[0]
@apis = "http://osu.ppy.sh/web/osu-osz2-getscores.php"\
        "?i=%s&c=%s&us=#{conf[:name]}&ha=#{conf[:pass]}"

def get_scores id, hash
  uri = URI.parse @apis % [id, hash]
  response = Net::HTTP.get uri
end

# puts get_scores *ARGV

Dir.chdir song_dir
folders = Dir.glob "*/"
folders.each{ |f|
  next unless song_id = f[/^\d+]
  song_path = File.join(song_dir, f)
  Dir.chdir song_path
  maps = Dir.glob "*.osu"
  maps.map!{|m| File.join song_path, m}
  if maps.length > 0
    maps_db.insert({})
    songs_db.insert({:song_id => song_id, :path => song_path})
  end
}

folders.select!{|f| f[/^\d+/]}
ids = folders.map{|f| f[/^\d+/]}
list = ids.zip(folders.map{|f| {dir:File.join(song_dir, f)}}).to_h

puts list

