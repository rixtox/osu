require 'net/http'
require 'uri'
require 'nokogiri'

class BeatMapNotExist < Exception
end

class Map

  attr_accessor :id, :artist, :title

  @@map_uri = "http://osu.ppy.sh%s"
  
  def initialize(map_uri)
    puts "New map: #{map_uri}"
    @map_uri = map_uri
    parse_html!
  end

  def exist?
    return @exists unless @exists.nil?
    not_found_node = html.css ".mainbody .mainbody2 .content .content-with-bg .paddingboth h2"
    @exists = !(!not_found_node.empty? and
                not_found_node[0].text[/The beatmap you are looking for was not found/])
  end

  private

  def url
    "http://osu.ppy.sh#{@map_uri}"
  end

  def html
    return @html if @html
    @raw_html = Net::HTTP.get(URI.parse url)
    @html = Nokogiri::HTML(@raw_html)
  end

  def parse_html!
    raise BeatMapNotExist unless exist?

    @id = get_id
    @title = get_title
    @artist = get_artist
  end
  
  def get_title
    html.css("#songinfo td")[7].text.strip
  end

  def get_id
    html.css("a[onclick^='return play']")[0][:onclick][/(?<=return play\()\d+/]
  end

  def get_starnum n
    td = html.css("#songinfo td")[n]
    field = td.css(".starfield")[0][:style][/(?<=width:)\d+/].to_i
    active = td.css(".active")[0][:style][/(?<=width:)\d+/].to_i
    active * 10.0 / field
  end

  def get_artist
    html.css("#songinfo td")[1].text.strip
  end

  def get_ar
    get_starnum 5
  end

  def get_hp
    get_starnum 9
  end

  def get_star
    get_starnum 11
  end

  def get_acc
    get_starnum 15
  end

  def get_cs
    get_starnum 3
  end

end
