class MapLib
  attr_accessor :songdir, :dldir

  def initialize(conf)
    @songdir = conf[:songdir]
    @dldir = conf[:dir]
  end

  def exist?(id)
    if @songdir
      Dir.chdir @songdir
      return true unless Dir.glob("#{id} *").empty?
    end
    Dir.chdir @dldir
    !Dir.glob("#{id} *").empty?
  end
end